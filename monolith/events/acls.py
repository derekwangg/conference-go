from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    header = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query": f"{city} {state}"}
    url = f"https://api.pexels.com/v1/search"
    response = requests.get(url, headers=header, params=params)
    picture = json.loads(response.content)
    picture_url = {"picture_url": picture["photos"][0]["src"]["original"]}
    try:
        return picture_url
    except:
        return {"picture_url": None}


def get_weather_data(city, state):
    params = {
        "q": city +","+state+",usa",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    url = f"https://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    weather = json.loads(response.content)
    w_lat = weather[0]["lat"]
    w_lon = weather[0]["lon"]
    url2 = "https://api.openweathermap.org/data/2.5/weather"
    param = {
        "lat": w_lat,
        "lon": w_lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    response2 = requests.get(url2, params=param)
    weather2 = json.loads(response2.content)
    weather_data = {
        "temp": (weather2["main"]["temp"]),
        "weather_description": weather2["weather"][0]["description"],
    }
    return weather_data
